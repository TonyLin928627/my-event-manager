package com.example.myeventmanager

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.Log
import com.example.myeventmanager.firebase.RealTimeDBHelper
import com.example.myeventmanager.utils.*
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Semaphore

class App : Application() {
    companion object {
        @JvmStatic
        var  INSTANCE: App? = null
    }

    override fun onCreate() {
        super.onCreate()

        INSTANCE = this

        PreferencesHelper.install(this)

        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener{
            taskRst->
            when(taskRst.isSuccessful){
                true-> {
                    val token = taskRst.result?.token
                    val id = taskRst.result?.id

                    PreferencesHelper.instanceToken = token?:""
                    PreferencesHelper.instanceId = id?: "DefaultInstanceId"

                }
            }
        }
        RealTimeDBHelper.install()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "My Event Manager"
            val description = "Upcoming event notification"

            val notificationChannel =
                NotificationChannel(NEW_EVENT_NOTIFICATION_CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT)

            notificationChannel.description = description

            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED

            notificationChannel.enableVibration(true)
            notificationChannel.vibrationPattern = longArrayOf(100, 200, 300)

            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(notificationChannel)
        }
    }

    private var mProgressDialog: ProgressDialog? = null
    fun showProgressDialog(context: Context) {

        mProgressDialog?.dismiss()

        ProgressDialog(context).let {
            this.mProgressDialog = it
            it.setMessage(getString(R.string.main_dialog_progress_title))
            it.setCanceledOnTouchOutside(false)
            it.show()
        }

    }

    fun hideProgressDialog() {
        mProgressDialog?.dismiss()
        mProgressDialog = null
    }
}